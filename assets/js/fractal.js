import * as THREE from 'https://unpkg.com/three/build/three.module.js';
const scene = new THREE.Scene();
const aspect = 1;
const d = 1;
const camera = new THREE.OrthographicCamera( - d * aspect, d * aspect, 
    d, - d, 1, 10 );
const renderer = new THREE.WebGLRenderer( { canvas: fractal, alpha: true });
const container = renderer.domElement.parentElement;
container.addEventListener("resize", onContainerResize);
window.addEventListener("resize", onContainerResize);

const uniform = {
    time: { value : 1 },

    aspect: { value: 1},
    zoom: { value: 1 },
    fov: { value: 82},

    camPos: { value: new THREE.Vector3(0.6, 0, -3.1) },
    camDirection: { value: new THREE.Vector3(0 , Math.PI / 2, Math.PI )},
    
    fractalIteration: { value: 4 },
    fractalRotation: { value: new THREE.Vector3(0, 0, 0) },

    raymarchingIteration: { value: 100 },
    raymarchingMaxDist: { value: 10 },
    raymarchingMinDist : { value: 0.001},
};
const material = new THREE.ShaderMaterial({ 
    uniforms: uniform, 
    fragmentShader: fragmentShader, 
    vertexShader: vertexShader,
});

const geometry = new THREE.PlaneGeometry(d * 2 * aspect, d * 2);

const plane = new THREE.Mesh(geometry, material )

scene.add(plane);

camera.position.z = 3;

function onContainerResize() {
    var box = container.getBoundingClientRect();
    renderer.setSize(box.width, box.height);
}

const start = new Date();
var frame = 0;
function render() {
    const time = (new Date() - start) / 1000;
    if (time > 1 && frame < 30) {
        return;
    }
    requestAnimationFrame(render);
    uniform.time.value = time;
    renderer.render(scene, camera);
    frame += 1;
}
// console.log(vertexShader);
// console.log(fragmentShader);
onContainerResize();
render();
