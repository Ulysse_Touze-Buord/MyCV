varying vec4 vPos;

const float PI = 3.14159265359;
const vec4 COLOR_START = vec4(0.2, 0.5, 1., 1.);
const vec4 COLOR_END = vec4(0.3, 0.0, 0.4, 1.);
uniform float time;

uniform float aspect;
uniform float zoom;
uniform float fov;

uniform vec3 camPos;
uniform vec3 camDirection;

uniform int fractalIteration;
uniform vec3 fractalRotation;

uniform int raymarchingIteration;
uniform float raymarchingMaxDist;
uniform float raymarchingMinDist;

mat2 rotation(float angle) {
    float s = sin(angle);
    float c = cos(angle);
    return mat2(c, -s, s, c);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec4 lerp(float value, vec4 start, vec4 end) {
    return (1. - value) * start + end * value;
}

float map(float x, float xStart, float xEnd, float start, float end) {
    return start + (end - start) * (x - xStart) / (xEnd - xStart);
}

float DE_mandelabulb(vec3 start) {
    vec3 z = start;

    float n = 8. - cos(time / 5.) * 3.;
    float r = 0.0;
    float dr = 1.0;
    for (int i = 0; i < fractalIteration; i++) {
        r = length(z);
        if ( r > 2.) {
            break;
        }

        float phi = atan(z.y, z.x);
        float theta = asin(z.z / r);
        dr = pow(r, n - 1.0) * n * dr + 1.0;

        z = vec3(sin(n*theta)*cos(n*phi),
            sin(n*theta)*sin(n*phi),
            cos(n * theta)) * pow(r, n);
        z += start;
    }
    return 0.5 * log(r) * r / dr;
}

float circleDE(vec3 pos) {
    vec3 mod_pos = vec3(mod(pos.x, 2.5) - 1.0, mod(pos.y, 2.5) - 1.0, pos.z) - vec3(0.0, 0.0, -5.0);
    return length(mod_pos) - 1.0;
}

vec4 raymarching(vec3 start, vec3 direction) {
    float total_distance = 0.0;
    for (int i = 0; i < raymarchingIteration; i++) {
        vec3 pos = start + total_distance * direction;
        float distance = DE_mandelabulb(pos);
        total_distance += distance;
        if (distance < raymarchingMinDist){
            return lerp(1. - pow(float(i) / float(raymarchingMaxDist), 0.5), COLOR_START, COLOR_END);
        }
        
        if (distance > raymarchingMaxDist) {
            return vec4(0.);
        }
    }
    return vec4(0.);
}

void main() {
    vec2 vCoords = vPos.xy;
    vCoords /= vPos.w;
    vCoords = vCoords * 0.5 + 0.5;

    vec2 uv = fract(vCoords);
    vec2 screenPos = uv - vec2(0.5, 0.5);
    screenPos = vec2(aspect * screenPos.x, screenPos.y) * zoom;

    gl_FragColor = vec4( 0.0, 0., 0., 0. );
    
    vec3 direction = vec3(0, 1, 0);
    vec3 direction_x = vec3(1, 0, 0);
    vec3 direction_y = vec3(0, 0, 1);
    
    direction.xy *= rotation(camDirection.x);
    direction_x.xy *= rotation(camDirection.x);

    direction.yz *= rotation(camDirection.y);
    direction_y.yz *= rotation(camDirection.y);

    float half_fov = fov / 360. * PI;
    vec2 offset_vector = vec2(screenPos.x * tan(half_fov), 
        screenPos.y * tan(half_fov / (aspect)));
    offset_vector *= rotation(camDirection.z);

    direction += direction_x * offset_vector.x;
    direction += direction_y * offset_vector.y;
    
    float h = atan(direction.x, direction.y) / (PI * 2.);
    // gl_FragColor = vec4(hsv2rgb(vec3(h, 1, (direction.z + 1.) / 2.)), 1.0 );

    gl_FragColor = raymarching(camPos, direction);
}
