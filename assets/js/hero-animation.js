const element = document.querySelector(".welcome-text>ul");
function deleteContent() {
    let content = [];
    Array.from(element.children).forEach((e, i) => {
        content.push(e.innerHTML);
    });
    element.innerHTML = "";
    element.appendChild(document.createElement("li"))
    return content
}

function animateHero(content) {
    if (content[0] == "" || content[0] == undefined) {
        content = content.slice(1);
        if (content.length != 0)
            element.appendChild(document.createElement("li"))
    }
    if (content.length == 0) return;
    
    element.lastChild.innerHTML += content[0][0];

    content[0] = content[0].substring(1);
    setTimeout(() => {animateHero(content)}, 50 + Math.random() * 10 +
        (content[0] == "" ? 1000 : 0));
}


const content = deleteContent();

setTimeout(() => {animateHero(content)}, 1000);
