---
title: "ulysse.touze-buord.fr"
tags: []
sourceCode: "https://gitlab.com/Ulysse_Touze-Buord/MyCV"
thumbnail: "/thumbnail.png"
description: "Les coulisses du site que vous visitez actuellement"
---
[Hugo]: https://gohugo.io/
[GitHub]: https://gohugo.io/
[GitLab]: https://gitlab.com/
[Mandelbulb]: https://fr.wikipedia.org/wiki/Mandelbulb
[Mandelbro]: https://fr.wikipedia.org/wiki/Ensemble_de_Mandelbrot
[Tree.js]: https://threejs.org/
[Fragment shader]: https://fr.wikipedia.org/wiki/Shader
[raymarching]: https://en.wikipedia.org/wiki/Ray_marching
[page cachée]: /cv-file/




# Mes motivations

Étant à la recherche de travail, j'ai eu besoin d'un CV. J'aurais pu faire
de façon classique et utiliser un CV pré-fait trouvé sur internet, mais cela ne
me convenait pas. J'avais besoin d'un CV pour me présenter, mais aussi d'un
portefolio. Étant développeur, j'ai donc pris la décision évidente de me faire
un site web. Après pas mal de recherches, j'ai choisi de faire mon site avec
[Hugo].

# Pourquoi Hugo ?

[Hugo] permet de facilement créer des sites web statiques. Un site statique à
toujours le même contenu contrairement à certains sites comme YouTube qui bouge
à chaque fois que l'on rafraichit la page. Mon site est simplement une
collection de fichiers accessibles via le protocole HTTP. Cela permet de
l'héberger gratuitement sur [GitHub] ou [GitLab] par exemple. Créer un site
comme ça à la main est assez difficile et peu robuste, car certaines pages se
ressemblent beaucoup et partagent du code en commun. Cela m'obligerait à faire
de la répétition de code. [Hugo] me permet d'écrire du code propre décrivant
mon site, le logiciel va par la suite prendre tous ces fichiers pour générer un
site statique.

<!-- # Comment ça fonctionne -->
<!---->
<!-- Le site est composé de plusieurs de ficher qui ont chacun leur utilité. On aura -->
<!-- les ficher de contenu qui sont des fichiers Markdown, qui serve à écrire le contenu des pages, l'on va ensuite crée le HTML dans le dossier layout -->
<!-- cela va nous permettre de décrire comment les page de contenue vont s'afficher, -->
<!-- dans assets nous pourrons mettre le CSS, le JS et autre contenue. Il reste -->
<!-- pour finir le dossier config qui nous permettra de simplement modifier des -->
<!-- configuration du site en yaml. -->
<!---->
<!-- Il existe beaucoup de template de site déjà fait sur hugo disponible sur -->
<!-- internet. Il permete de rapidement crée un site simplement en modifiant le -->
<!-- contenue des pages. Mais pour ma pare j'ai décider de crée cette template de -->
<!-- zero ce qui mas permis de comprendre en profondeur comment fonctione Hugo.  -->

# Mon Site

## L'architecture

Mon site est composé d'une page d'accueil qui possède plusieurs sections
prédéfinies. Le contenu de chacune de ces sections est configurable dans le
fichier conçu à cet effet : nom, prénom, présentation, contact, compétence...

On peut par la suite créer le portefolio en Markdown, ces fichiers seront
convertis en HTML et seront accessibles dans la page Projets.

## 3D Mandelbrot

On voit sur la page d'accueil une figure géométrique en 3D nommée [Mandelbulb],
elle est une façon de représenter la fractale de [Mandelbrot] en 3D. Cette
animation a été créée avec [Tree.js], cependant la 3D que fourni ce package n'a
pas été utilisé pour créer la figure, j'ai simplement utilisé un [Fragment
shader], La position et la direction de la caméra sont fournies au shader. J'ai
par la suite utilisé une technique appelée le [raymarching]
pour afficher cette figure.

## CV Papier

Après avoir fait mon site, j'ai voulu avoir une version papier, mais je voulais
que ce CV Papier corresponde aux informations de mon site web et à sa charte
graphique. Pour cela, j'ai décidé de créer ce CV toujours avec [Hugo].

Il existe sur le site une [page cachée], sur cette page, on y retrouve
une page HTML au format A4 qui a pour but d'être imprimée. J'ai par la suite
utilisé la fonctionnalité CI/CD de [GitLab] pour compiler et déployer mon site.
Après la compilation, un script est lancé et va convertir la page HTML de mon CV
en PDF et va mettre le fichier au bon endroit.

Donc toutes les données de mon site sont liées avec celles de mon CV papier et à
chaque changement de mon site mon CV change aussi.

