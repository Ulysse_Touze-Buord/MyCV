---
title: "Raycaster-C"
tags: ["C"]
sourceCode: "https://github.com/Bubulleux/Raycast-C"
thumbnail: "/screenshot_3.png"
description: "Une simple démo technique d’un moteur 3D similaire à celui de Doom 64."
---

[MLX]: https://github.com/42Paris/minilibx-linux
[42]: https://fr.wikipedia.org/wiki/42_(%C3%A9coles) 
[X11]: https://fr.wikipedia.org/wiki/X_Window_System
[Raycasting]: https://fr.wikipedia.org/wiki/Raycasting
[Wolfenstein 3D]: https://fr.wikipedia.org/wiki/Wolfenstein_3D
[Doom]: https://fr.wikipedia.org/wiki/Doom_64
[interpolation linéaire]: https://fr.wikipedia.org/wiki/Interpolation_lin%C3%A9aire
[Raycast-gif]: https://upload.wikimedia.org/wikipedia/commons/e/e7/Simple_raycasting_with_fisheye_correction.gif?uselang=fr

# Présentation

Raycaster-C est une petite démo d'un moteur 3D. Le programme est entièrement
écrit en C. Pour le rendu des images, il utilise le principe de [Raycasting]. Pour
m'aider dans ma tâche, j'ai utilisé la librairie [MLX].  

Le programme n'a pas vraiment de but : vous êtes simplement dans un monde à ciel
ouvert avec des murs qui vous entourent de différents types. Vous pouvez vous y
déplacer et regarder autour de vous.

## Fonctionnement

Le moteur fonctionne sur le principe de [Raycasting]. [Wolfenstein 3D] est
considéré comme le premier jeu ayant utilisé cette méthode de rendu mais le
[Raycasting] est plus connu pour avoir été utilisé dans le moteur de [Doom].  

Le principe est plutôt simple. On va créer un plan composé de segments qui
représenteront nos murs. Par la suite, on va lancer des rayons dans la
direction vers laquelle on regarde pour chaque colonne de pixel de notre écran.
Puis, on calcule le segment le plus proche avec lequel le rayon intersecte.
Ensuite, on peut calculer à quelle distance se trouve le mur. Il suffit
d'utiliser de la trigonométrie pour calculer quelle taille doit avoir le mur
sur la colonne de pixel. Nous répéterons cette opération jusqu'à remplir
l'écran. En fonction de la direction du segment que l'on a intersecté, on peut
connaitre la quantité de lumière qu'il reçoit. On peut ensuite modifier la
luminosité de la texture du mur (assombrir ou éclaircir) pour encore ajouter
un effet de 3D. Avec encore plus de trigonométrie, on peut ajouter une texture
au sol. On peut aussi facilement implémanter du brouillard en faisant une
[interpolation linéaire] entre la couleur que doit avoir le mur et la couleur
du brouillard en fonction de la distance de ce que l'on regarde.  

Avec tous ces effets on obtient une impression de 3D assez satisfaisante.

## MLX

[MLX] autrement appeler MiniLibX est une simple librairie permettant de
facilement créer une fenêtre dans laquelle nous pourrons, dans un second temps,
afficher des choses et récupérer les entrées clavier et souris. Elle a été
créée par et pour les étudiant·es de l'école [42]. Cette librairie est une
version allégée de la très connue librairie [X11] qui permet de créer des
fenêtres au même titre que [MLX] mais de façon beaucoup plus complète.

# Les limitations

À cause du fonctionnement du moteur, le joueur ne peut pas modifier la hauteur
de la caméra, il ne peut donc ni sauter ni bouger sa caméra verticalement.

Le second problème est que le programme fonctionne entièrement sur un seul cœur
du processeur et non sur la carte graphique. De plus, comme il sagit d'un de
mes tous premiers projets écrits en C, le programme n'est pas très bien
optimisé. Il peut donc y avoir quelques ralentissements sur certains
ordinateurs peu puissants.

# Ce que ce projet m'a appris

Ce projet n'a pas pour objectif de créer un outil de développement de jeux
vidéo mais simplement de me challenger. Il m'a permis d'apprendre à utiliser le
C qui est un langage très bas niveau, ce qui m'a fait comprendre beaucoup de
choses sur le fonctionnement d'un ordinateur. Il m'a aussi permis d'en
découvrir plus sur le rendu graphique qui est un sujet très vaste et très
intéressant.

# Captures d'écran

{{<gallery 
    "./screenshot_7.png"
    "./screenshot_3.png"
    "./screenshot_4.png"
    "./screenshot_5.png"
    "./screenshot_6.png"
    "./screenshot_1.png"
    "./screenshot_8.png"
>}}
