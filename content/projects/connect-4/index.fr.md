---
title: "Connect 4 IA"
tags: ["C++"]
sourceCode: "https://github.com/Bubulleux/Connect4-ia-Cpp"
thumbnail: "/screenshot_1.png"
description: "Une IA qui joue au Puissance 4"
---

[Minimax]: https://fr.wikipedia.org/wiki/Algorithme_minimax
[Table de hachage]: https://fr.wikipedia.org/wiki/Table_de_hachage
[Puissance 4]: https://fr.wikipedia.org/wiki/Puissance_4

# Présentation

Connect 4 AI est un petit projet écrit en C++. Comme dit dans le titre, le
projet est une IA qui joue au jeu du [Puissance 4]. Le Puissance 4 se joue en
un contre un, les deux joueurs jouent chacun leur tour en faisant tomber un jeton
dans une colonne du tableau de jeux. La première personne qui arrive à aligner 4
jetons gagne. 

![Image d'un tableau de Puissance 4](./connect_4_ilustration.png)

*Image venant de Wikipedia.*

Le fonctionnement du programme est basé sur l'algorithme [Minimax].

# Le fonctionnement de Minimax

Le programme va premièrement calculer tous les coups possibles après un
certain nombre de coups, ce qui représente très vite une grande quantité de coups.
Par exemple, pour 7 coups joués, le nombre de jeux possibles est de 7⁷ soit
environ 800 000 jeux.

La deuxième étape va être de déterminer quelle est la meilleure stratégie en
commençant par les coups les plus profonds et en remontant dans l'arbre en
déterminant à chaque étape lequel est le plus favorable pour le joueur qui joue.

# Comment améliorer le programme ?

Pour améliorer le programme et le rendre plus performant, il nous est offert
plusieurs optimisations possibles.

- On peut premièrement rendre le programme multi-cœur : cette amélioration est
  basique mais très compliquée à implémenter, car l'on ne travaille pas sur un
  problème avec des données isolées. Ici on travaille avec un arbre de données
  où chaque nœud est relatif à son parent.

- La deuxième optimisation vient du fait que l'on calcule souvent 2 fois le
  même état de jeu, car on peut facilement arriver à un même état en simplement
  jouant les coups dans un ordre différent. Pour cela, on peut stocker chacun
  des états dans une [Table de hachage] mais cela implique des complications si
  l'on doit en même temps faire tourner le programme sur plusieurs cœurs du
  processeur.

- La dernière optimisation est nullement négligeable : c'est le fait de
  sélectionner certains états de jeux qui paraissent prometteurs pour leur
  donner davantage de temps de calcul et éviter de passer trop de temps avec
  certains coups qui n'ont aucun avenir. Le problème avec cela, c'est que l'on
  ne peut pas savoir si un coup est prometteur ou non sans avoir pris du temps
  à le calculer. Certain coups peuvent paraître mauvais mais sont en réalité
  être très bons et vice versa. Toute la difficulté réside dans l'équilibrage
  du temps de calcul entre les coups.

# Démonstration

L'interface console n'est pas du tout finie et est surtout là pour le débogage.
Mais voici à quoi ressemble une partie contre l'IA: 
[Fichier console](./console.txt)
