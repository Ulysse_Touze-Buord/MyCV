---
title: "Bacterio"
tags: ["Unity", "Csharp"]
sourceCode: "https://github.com/Bubulleux/Bacterio"
download: "https://github.com/Bubulleux/Bacterio/releases/latest"
thumbnail: "/screenshot_2.png"
description: "Un petit jeu réalisé en 48H qui reprend les codes des jeux .IO"
---

[Unity]: https://fr.wikipedia.org/wiki/Unity_(moteur_de_jeu)
[Tuto Unity FR]: https://www.tutounity.fr/index.php
[agar.io]: https://www.agar.io/
[slither.io]: http://slither.io/

# Présentation

Bacterio c'est un petit jeu crée sous [Unity] en 48h à l'occasion de la Game Jam
2020 de [Tuto Unity FR].

## Le principe du jeu

Le thème de la Game Jam était: "**Vous ne commencez avec rien**"

J'ai donc décidé de faire un jeu style “.io” ([agar.io], [slither.io]) où le
joueur est une bactérie et doit évoluer en tuant d'autres bactéries. Le joueur
apparait donc dans un monde vu de haut où des cubes verts sont répartis sur le
sol. Ces cubes verts représentent de l'expérience pour le joueur et lui
permettent d'améliorer sa bactérie de façon à tuer d'autres bactéries pour
avoir plus d'expérience et pouvoir tuer encore plus de bactéries.

À cause du temps limité de 48h et du fait que j'étais seul sur le jeu, je n'ai
pas eu le temps d'implémenter un multijoueur. Les autres bactéries du monde
sont donc jouées par des robots qui ont plusieurs comportements possibles :
soit ils vont venir au contact du joueur, mais ils auront maximisé leur vie,
soit ils seront plutôt à longue distance et dans ce cas-là ils auront maximisé
leur attaque et leur vitesse.

# Captures d'écran

{{<gallery
    "./screenshot_1.png"
    "./screenshot_2.png"
>}}

{{< video "./gameplay.mp4" >}}
