---
title: "Raspberry Dashboard"
tags: ["nodejs", "web"]
sourceCode: "https://github.com/Bubulleux/Raspberry-Dashboard"
thumbnail: "/screenshot_1.png"
description: "Un dashboard dans le but de monitorer un Raspberry Pi"
---

[Raspberry Pi]: https://fr.wikipedia.org/wiki/Raspberry_Pi
[SSH]: https://fr.wikipedia.org/wiki/Secure_Shell

# Présentation

Raspberry Dashboard est une interface web à la base conçue pour monitorer un
[Raspberry Pi], même si elle peut facilement être portée sur n'importe
quel OS Linux. Elle est faite avec nodejs et quelques paquets npm.

## Dashboard

Le premier menu est simplement un tableau d'informations. Il affiche certains
renseignements essentiels comme le modèle du Raspberry Pi, son système
d'exploitation, la température et la charge du CPU. Il permet aussi d'éteindre
ou de redémarrer le Raspberry Pi.

![Page d'informations](./screenshot_1.png)

## Explorateur de fichiers

Ce panneau est à mon avis le plus utile et le mieux réussi. Le menu File
Explorer implémente un gestionnaire de fichiers à l'intérieur du navigateur. On
peut y faire toutes les actions basiques et essentielles d'un explorateur de
fichiers (Ajouter un dossier, renommer un fichier, supprimer, couper, copier,
coller, etc.). On peut également télécharger et uploader des fichiers. Enfin,
une fonctionnalité permet la création de raccourcis.

![Page d'informations](./screenshot_3.png)

## Console

Le dernier menu est une tentative d'implémenter un terminal dans un navigateur.
Même si l'on peut faire quelques commandes basiques, son utilisation reste assez
compliquée, il est donc préférable d'utiliser un client [SSH].

![Page d'informations](./screenshot_2.png)

