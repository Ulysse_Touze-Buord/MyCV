---
title: "Better Bus"
tags: ["Dart", "Flutter"]
sourceCode: "https://github.com/Bubulleux/better-bus-v2"
download: "https://play.google.com/store/apps/details?id=com.bubulle.better_bus_v2"
thumbnail: "/thumbnail.png"
description: "Une application mobile ayant pour but de donner une alternative à application de Vitalis"
---

[l'application Vitalis]: https://play.google.com/store/apps/details?id=fr.catp.airweb.vitalis&hl=fr&gl=US&pli=1
[site Vitalis]: https://www.vitalis-poitiers.fr/
[GooglePlay]: https://play.google.com/store/apps/details?id=com.bubulle.better_bus_v2

# Better bus, qu'est-ce que c'est ?

Better Bus, c'est simplement une application mobile qui a pour but de nous
faciliter la vie quand on circule en bus à Poitiers sur le réseau Vitalis. Elle
a été développée comme une alternative à [l'application Vitalis] qui elle-même
a vocation à nous faciliter la vie. Cependant, Better Bus est beaucoup plus
simple d'utilisation, beaucoup plus belle et avec certaines fonctionnalités
très importantes que ne propose pas l'application Vitalis. 

# Ses fonctionnalités

Avec Better Bus vous pouvez :
- Consulter les fiches horaires (les horaires fixes des Bus), même hors connexion.
- Consulter les prochains passages à un arrêt de bus pour être informé.es des
  variations de l'heure d'arrivée des bus.
- Trouver facilement quel est le meilleur itinéraire pour aller d'un point A
  vers un point B de Grand Poitiers.
- Accéder aux Infos trafic du réseau de bus pour toujours être au courant en
  cas de retard ou de déviation.
- Recevoir des notifications quand une nouvelle Info Trafic apparaît. Vous
  serez averti.es quand l'Info trafic apparaît, quand elle est modifiée et
  quand elle devient active.

La plupart de ces fonctionnalités citées sont déjà disponibles sur
l'application Vitalis, mais d'une façon qui, je trouve, est beaucoup moins
ergonomique, moins pratique et moins esthétique.

À mon avis, il manque une fonctionnalité essentielle dans l'application
Vitalis : la possibilité de créer des raccourcis vers des informations
consultées très régulièrement pour ne pas avoir à refaire à chaque fois la même
recherche. Dans mon application, il suffit de donner un arrêt de bus, une ligne et
sa direction. Par la suite, ce raccourci apparaîtra dans le menu principal via
un bouton qui amène directement à l'information voulue. Il y a aussi la
possibilité d'ajouter un widget sur la page d'accueil de votre téléphone pour
rendre l'opération encore plus rapide. Certes, c'est une optimisation qui
paraît être minime, mais je la trouve personnellement indispensable quand elle
concerne une action que l'on peut faire deux à trois fois par jour dans un
contexte de potentiel stress où l'on ne sait pas si on arrivera à prendre ce
bus à temps.

# Les limitations

Toutes les données affichées sur Better Bus sont fournies par le [site
Vitalis]. S'il y a une erreur sur leur application, il y en aura aussi une sur
Better Bus. Si les serveurs de Vitalis sont en panne, alors mon application ne
fonctionnera plus. Si une mise à jour majeure est effectuée sur leur serveur
alors Better Bus risque d'avoir des difficultés pour accéder à nouveau aux
données.

La seconde limitation majeure c'est l'impossibilité d'acheter des billets de
bus sur Better Bus. Ce qui peut être dérangeant pour un certain nombre de
personnes.

# Captures d'écran
{{<gallery 
    "./better-bus-v2-0.png"
    "./better-bus-v2-1.png"
    "./better-bus-v2-2.png"
    "./better-bus-v2-3.png"
    "./better-bus-v2-4.png"
    "./better-bus-v2-5.png"
    "./better-bus-v2-6.png"
>}}

# Télécharger

{{< googleplaybtn "https://play.google.com/store/apps/details?id=com.bubulle.better_bus_v2" >}}


